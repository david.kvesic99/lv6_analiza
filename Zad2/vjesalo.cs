﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

/*Napravite jednostavnu igru vješala. Pojmovi se učitavaju u listu iz datoteke, i u
svakoj partiji se odabire nasumični pojam iz liste. Omogućiti svu
funkcionalnost koju biste očekivali od takve igre. Nije nužno crtati vješala,
dovoljno je na labeli ispisati koliko je pokušaja za odabir slova preostalo.*/


namespace Zad2
{
    public partial class LabelRijec : Form
    {
        public LabelRijec()
        {
            InitializeComponent();
        }
        string path = "C:\\Users\\Kves\\source\\repos\\WindowsFormsApp1\\Zad2\\rjesenje.txt";
        Random rand = new Random();
        List<string> lista = new List<string>();
        int brojPokusaja=7;
        string rijec,hide;

        public void reset()
        {
            brojPokusaja = 7;
            int random = rand.Next(0, lista.Count);
            rijec = lista[random];
            hide = new string('*', rijec.Length);
            label2.Text = hide;
            
            labelPok.Text = "Broj pokusaja: " + brojPokusaja.ToString();

        }
        private void LabelRijec_Load(object sender, EventArgs e)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(@path))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {


                    lista.Add(line);
                }
                listBox1.DataSource = null;
                listBox1.DataSource = lista;
                listBox1.Hide();
                reset();
            }

        }


        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == rijec)
            {
                MessageBox.Show("Cestitam ,pobijedili ste!");
                reset();
            }

            else if (textBox1.Text.Length == 1)
            {
                if (rijec.Contains(textBox1.Text))
                {
                    StringBuilder temp = new StringBuilder(hide);

                    for (int i = 0; i < rijec.Length; i++)
                    {
                        if (rijec[i].ToString() == textBox1.Text)
                        {
                            temp[i] = Convert.ToChar(textBox1.Text);
                        }
                    }
                    hide = temp.ToString();
                    label2.Text = hide;
                    textBox1.Clear();

                    if (hide == rijec)
                    {
                        MessageBox.Show("Cestitam ,pobijedili ste!");
                        reset();
                    }
                }
                else
                {
                    brojPokusaja--;
                    labelPok.Text = "Broj pokusaja: " + brojPokusaja.ToString();
                    textBox1.Clear();

                    if (brojPokusaja == 0)
                    {
                        MessageBox.Show("Izgubili ste!");
                        reset();
                    }

                }
            }
            else
            {
                brojPokusaja--;
                labelPok.Text ="Broj pokusaja: "+ brojPokusaja.ToString();
                textBox1.Clear();
                if (brojPokusaja == 0)
                {
                    MessageBox.Show("Izgubili ste!");
                    reset();
                }

            }
            

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void labelPok_Click(object sender, EventArgs e)
        {

        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        
    }
}
